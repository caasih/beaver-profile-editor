export const MAPBOX_ACCESS_TOKEN = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;

export const GIS_SERVICE = process.env.REACT_APP_GIS_SERVICE;
