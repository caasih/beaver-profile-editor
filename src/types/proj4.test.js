import { coordsToLatLng } from './proj4';

describe('proj4', () => {
  describe('coordsToLatLng', () => {
    it('should transform a coordinates in TWD97 to a coordinates in WGS84', () => {
      const lnglat =
        coordsToLatLng([284220.579639783, 2765430.11735139]);
      expect(lnglat)
        .toEqual([121.3390080865284, 24.99647688826665]);
    });
  });
});
