export interface Layout {
  padding: number;
  width: number;
  height: number;
}
