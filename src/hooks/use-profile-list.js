import { useMemo } from 'react';
import { FeatureCollection } from '@/types/geojson';
import data from './data.json';

export default function useProfileList() {
  return useMemo(() => {
    const features = data.map(f => f.content);
    return FeatureCollection.toLatLng({
      type: 'FeatureCollection',
      features,
    });
  }, []);
}
