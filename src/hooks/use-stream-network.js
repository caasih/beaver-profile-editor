import { useMemo } from 'react';
import { FeatureCollection } from '@/types/geojson';
import data from './stream_network.json';

export default function useStreamNetwork() {
  return useMemo(() => FeatureCollection.toLatLng(data), []);
}
