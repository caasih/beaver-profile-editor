import React from 'react';
import cx from 'classnames';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import EditorPage from '@/pages/Editor';
import styles from './index.module.scss';

export default function App({ id, className }) {
  const classes = cx('h2e-app', styles.className, className);

  return (
    <Router basename="/">
      <div id={id} className={classes}>
        <main>
          <Switch>
            <Route path="/">
              <EditorPage />
            </Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}
