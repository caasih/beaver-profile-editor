#!/usr/bin/env node
const fs = require('fs-extra');
const path = require('path');

async function main(argv) {
  const [,, dir] = argv;
  const cwd = path.resolve(process.cwd(), dir);

  try {
    let result = [];
    let files = await fs.readdir(cwd);
    files = files.filter(f => f.substring(f.length - 4) === 'json');
    for (let filename of files) {
      const content = await fs.readJson(path.resolve(cwd, filename));
      result.push({ filename, content });
    }
    console.log(JSON.stringify(result, null, 2));
  } catch (err) {
    console.error(err);
  }
}

main(process.argv);
